# ft_server

Initialisation aux bases de l’administration système et réseau. Installation d'un serveur web complet, a l'aide de Docker.

Services mis en place :
- A web server (Nginx) that can be access in http / https
- Phpmyadmin
- Wordpress
- Mysql

Skills:
- Rigor
- Network & system administration 

Lire le [sujet][1]

`Prérequis : Docker`

### Compiler et lancer le projet

1. Installer [Docker][2]

2. Télécharger / Cloner le projet
    
```
git clone https://gitlab.com/flmarsil42/ft_server.git
```

3. `cd` dans le répertoire racine, et construisez le `Dockerfile`. Ensuite, exécutez l'image docker construite.
Vous devez mapper les ports 80 et 443 du conteneur docker vers le localhost en utilisant l'option `-p` 
```
cd ft_server
docker build -t ft_serv_img .
docker run -p 80:80 -p 443:443 ft_serv_img
```

4. Accédez aux différents services avec http:localhost/service avec votre navigateur, ou utilisez la commande docker exec -ti pour entrer dans le conteneur docker et voir les différents fichiers de configuration.
```
docker exec -it "container id" /bin/sh
```

## Sources

- [Full tutorial for Docker][3]
- [Tutoriel Docker (FR)][4]
- [Un autre tutoriel Docker (FR)][8]
- [Understanding how Wordpress works with Mysql][5]
- [Dockerfile syntax][6]
- [Comprendre le fonctionnement d'un serveur web (FR)][7]

[1]: https://gitlab.com/flmarsil42/ft_server/-/blob/master/fr.subject.pdf
[2]: https://docs.docker.com/get-docker/
[3]: https://www.youtube.com/watch?v=jPdIRX6q4jA&list=PLy7NrYWoggjzfAHlUusx2wuDwfCrmJYcs&ab_channel=TechWorldwithNana
[4]: https://www.wanadev.fr/23-tuto-docker-comprendre-docker-partie1/
[5]: https://premium.wpmudev.org/blog/mysql-databases/
[6]: https://docs.docker.com/engine/reference/builder/
[7]: https://www.youtube.com/watch?v=msB9AvJ4bTM&ab_channel=Cookieconnect%C3%A9
[8]: https://devopssec.fr/article/differences-virtualisation-et-conteneurisation
